import { useEffect, useReducer } from 'react';
import './App.css';
import TaskList from './components/TaskList.js';
import defaultTasks from './utils/defaultTasks.js';
import TaskForm from './components/TaskForm.js';
import check from './assets/circle-check-solid-static.svg';

function App() {
  function getStoredTasks() {
    const prevTasks = JSON.parse(localStorage.getItem('listapp'));
    return prevTasks || defaultTasks;
  }
  const [tasks, dispatch] = useReducer(reducer, getStoredTasks());

  useEffect(() => {
    let newT = document.querySelector(".newTask");
    let check = document.querySelector(".icCheck");
    newT.style.display = "none";
    check.style.display = "block";
    setTimeout(() => {
      newT.style.display = "flex";
      check.style.display = "none";
    }, 500);
    localStorage.setItem("listapp", JSON.stringify(tasks));
  }, [tasks]);

  function reducer(tasks, action) {
    switch (action.type) {
      case "add":
        let nextId = 1;
        if (tasks.length) {
          nextId = tasks[tasks.length - 1].id + 1;
        }
        return [
          ...tasks,
          {
            id: nextId,
            nombre: action.nombre,
            completada: false,
          }
        ];
      case "delete":
        return tasks.filter((t) => t.id !== action.id);
      case "modify":
        tasks.filter((t) => {
          if (t.id === action.id) {
            // Horrible hack para evitar ejecutarse dos veces seguidas, necesita debugging.
            if ((t.completada === "true" || t.completada === true) && action.count % 2 === 0) {
              t.completada = false;
              action.count++;
            } else if (action.count % 2 === 0) {
              t.completada = true;
              action.count++;
            }
            return true;
          }
        });
        return [...tasks];
      default:
        break;
    }
  }
  
  function handleAddTask(name) {
    dispatch({
      type: 'add',
      nombre: name,
    });
  }

  function handleDeleteTask(id) {
    dispatch({
      type: 'delete',
      id: id,
    });
  }

  function handleModifyTask(id) {
    dispatch({
      type: 'modify',
      id: id,
      count: 0
    });
  }

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Te damos la bienvenida a <i>Listapp! </i> &nbsp; Tu app de tareas.
        </p>
      </header>
      <main className='App-main'>
        <img src={check} alt="icono de ok" className='icCheck'/>
        <TaskForm tasks={tasks}
          onAddTask={handleAddTask}
        />
        <TaskList tasks={tasks}
          onDeleteTask={handleDeleteTask}
          onModifyTask={handleModifyTask}
        />
      </main>
    </div>
  );
}

export default App;
