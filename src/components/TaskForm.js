import { useState } from "react";

const TaskForm = (props) => {
	const [nombre, setNombre] = useState("");
	
	const handleSubmit = (e) => {
		e.preventDefault();
		props.onAddTask(nombre);
		setNombre("");
	}
	return(
		<div className="newTask">
			<h2>Agregar nueva tarea</h2>
			<form className="newTaskForm" onSubmit={handleSubmit}>
				<label>Tarea: </label>
				<br/>
				<input required type="text" className="inTask" value={nombre}
					onChange={(e) => setNombre(e.target.value)}/>
				<br/>
				<button type="submit" className="btn btnSubmit">Agregar</button>
			</form>
		</div>
	);
}

export default TaskForm;
