import { useState } from "react";
import '../css/style.css';
import icEliminar from '../assets/trash-can-regular.svg';
const TaskItem = (props) => {
	const task = props.task;
	const onDeleteTask = props.onDeleteTask;
	const onModifyTask = props.onModifyTask;
	const [completada, setCompletada] = useState((task.completada === "false" || task.completada === false) ? false : true);

	function handleTaskCompletion() {
		setCompletada(!completada);
		onModifyTask(task.id);
	}

	return(
		<li className="element">
			<button className="btn" onClick={() => onDeleteTask(task.id)}>
				<img src={icEliminar} alt="icono de eliminar" className="ic"/>
			</button>
			{(completada) ? <>
				<button className="btn btnDone" onClick={handleTaskCompletion}> Desmarcar </button>
				<span className="nombreTarea tareaDone">{task.nombre}</span>
				</> : <>
				<button className="btn btnPending" onClick={handleTaskCompletion}> Completar </button>
				<span className="nombreTarea tareaPending">{task.nombre}</span>
				</>
			}
		</li>
	);
}

export default TaskItem;
