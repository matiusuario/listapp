import TaskItem from "./TaskItem.js";

const TaskList = (props) => {
	return(
		<div className="myTasks">
			<h2>Mis tareas</h2>
			<ul>
				{props.tasks.map((t) => <TaskItem task={t} key={t.id} onDeleteTask={props.onDeleteTask} onModifyTask={props.onModifyTask}/>)}
			</ul>
			{(props.tasks.length === 0) && <span>No hay ninguna tarea. Prueba agregando una</span>}
		</div>
	);
}

export default TaskList;
