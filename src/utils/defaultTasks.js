const defaultTasks = [{
	"id": "1",
	"nombre": "Hacer compras",
	"completada": "false"
}, {
	"id": "2",
	"nombre": "Lavar la ropa",
	"completada": "true"
}, {
	"id": "3",
	"nombre": "Saludar por el cumpleaños a...",
	"completada": "false"
}, {
	"id": "4",
	"nombre": "Terminar el trabajo práctico",
	"completada": "false"
}];

export default defaultTasks;
